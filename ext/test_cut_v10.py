from pox.core import core
from pox.lib.revent.revent import EventMixin
import pox.openflow.libpof_02 as pof
import pox.openflow.bypassmanager as bpm
from _multiprocessing import flags
global dpid1
global dpid2
global dpid3
global dpid4
dpid1 = 4
dpid2 = 3
#3 link east
dpid3 = 2
#4 link dsp
dpid4 = 1
global rates
global meter_id
global src
global flag2
flag2 = 0
global flag1
flag1 = 0
src1 = 'c0a81305'
#src1 link the meter
src2 = '0a000003'
rates = 10000
meter_id = 0

global limited_bw
limited_bw = 4.0
global bw_record
bw_record = []

global pretime
global prebyte_value1
global prebyte_value2
pretime = 0
prebyte_value1 = -1
prebyte_value2 = -1
global increasing
increasing = 0.1
global circle_time
circle_time =0
global MAX
MAX = 10
global bw_now
bw_now = 0

def _timer_func ():
    global dpid2
    global dpid1
    global flag1
    global flag2
    global rates
    global meter_id
    global limited_bw
    global bw_record
    global increasing
    global circle_time
    global MAX
    global pretime
    global prebyte_value1
    global prebyte_value2
    global bw_now
    msg = pof.ofp_counter_request()
    msg.counter.counter_id = 1
    msg.counter.command = pof.OFPCC_QUERY
    #if flag2 == 1:
    #changed to dpid1
    if flag1 == 1:
        core.PofManager.write_of(dpid1,msg)
    else:
        print 'query id 1, but dpid1 no connect yet'

    msg.counter.counter_id = 2
    if flag1 == 1:
        core.PofManager.write_of(dpid1,msg)
    else:
        print 'query id 2, but dpid1 no connect yet'
        
    #handle the reply
    if flag1 == 1:
        if (bpm.rate_flag1 != "") and (bpm.rate_flag2 != ""):
            #print "--",bpm.rate_flag.counter.last_time
            print "id--",bpm.rate_flag1.counter.counter_id
            print "cur--",bpm.rate_flag1.counter.byte_value
            print "pre--",prebyte_value1
            #curtime = bpm.rate_flag.counter.last_time
            curbyte_valude1 = bpm.rate_flag1.counter.byte_value
            
            print "id--",bpm.rate_flag2.counter.counter_id
            print "cur--",bpm.rate_flag2.counter.byte_value
            print "pre--",prebyte_value2
            #curtime = bpm.rate_flag.counter.last_time
            curbyte_valude2 = bpm.rate_flag2.counter.byte_value
            
            if prebyte_value1 == -1:
                print 'initial id 1'
                #print 'no packet 1'
                prebyte_value1 = curbyte_valude1 
            else:
                if prebyte_value1 == -1:
                    print 'initial id 2'
                    #print 'no packet 2'
                    prebyte_value2 = curbyte_valude2
                else:
                    bw_now1 = 1.0*(curbyte_valude1 - prebyte_value1)
                    bw_now1 = bw_now1*8.0/1024/1024
                    prebyte_value1 = curbyte_valude1
                    print 'bw_now1',bw_now1
                    
                    bw_now2 = 1.0*(curbyte_valude2 - prebyte_value2)
                    bw_now2 = bw_now2*8.0/1024/1024
                    prebyte_value2 = curbyte_valude2
                    print 'bw_now2',bw_now2
                    
                    bw_now = bw_now1 + bw_now2
                    
                    print 'bw_now',bw_now
                    
                    
                    if bw_now > 10.2:
                        if limited_bw + bw_now2 > 10.2:
                            print 'real bw_now',limited_bw + bw_now2
                            limited_bw = limited_bw * 0.8
                    
                    if limited_bw + bw_now2 < 9.7:
                        limited_bw += 0.1
                    # if bw_now > 0 and bw_now < limited_bw*0.95:
                        # bw_record.append(bw_now)
                    # else:
                        # bw_record = []
                    # if len(bw_record) >= 2:
                        # limited_bw = 1.0*sum(bw_record)/len(bw_record)
                        # bw_record = []
                    # circle_time += 1
                    # if circle_time>=2:
                        # circle_time = 0
                        # limited_bw += increasing
                        #print 'increasing:',increasing
                       
                        
                    if limited_bw > MAX:
                        limited_bw = MAX

                    print 'limited_bw:',limited_bw
                    
                    #limited_bw = MAX   
                    
                    rates = limited_bw * 1024
                    msg = pof.ofp_meter_mod()
                    msg.command = 1
                    msg.meter_id = meter_id
                    msg.rate = rates
                    print 'change the meter rate to ',rates
                    core.PofManager.write_of(dpid1,msg)
        else:
            print "do not receive counter reply"
        print "----------"
    else:
        print 'dpid1 no connect yet'
        


class TestCounter(EventMixin):
    def __init__ (self):
        self.add_protocol()
        core.openflow.addListeners(self, priority=0)

    def add_protocol(self):
        field_list = [("DMAC",48), ("SMAC",48), ("Eth_Type",16), ("V_IHL_TOS",16), ("Total_Len",16),
                      ("ID_Flag_Offset",32), ("TTL",8), ("Protocol",8), ("Checksum",16), ("SIP",32), ("DIP",32),
                      ("UDP_Sport",16), ("UDP_Dport",16), ("UDP_Len",16), ("UDP_Checksum",16)]
        match_field_list = []
        total_offset = 0
        for field in field_list:
            field_id = core.PofManager.new_field(field[0], total_offset, field[1])
            print "field_id: ", field_id
            total_offset += field[1]
            match_field_list.append(core.PofManager.get_field(field_id))
        print 'protocol_id: ', core.PofManager.add_protocol("ETH_IPV4_UDP", match_field_list)

    def _handle_ConnectionUp (self, event):
        global dpid1
        global dpid2
        global dpid3
        global dpid4
        global meter_id
        global rates
        global src1
        global src2
        global flag2
        global flag1
        #core.PofManager.set_port_of_enable(event.dpid, 1)
        #core.PofManager.set_port_of_enable(event.dpid, 2)
        #core.PofManager.set_port_of_enable(event.dpid, 3)
        print 'dpid connected: ',event.dpid
        #=======================================================================
        # msg = pof.ofp_counter_mod()
        # msg.counter.counter_id = 1
        # msg.counter.command = pof.OFPCC_ADD
        # core.PofManager.write_of(dpid,msg)
        # print 'write counter success'
        # src1 link the meter
        #=======================================================================
        if event.dpid==dpid1:
            meter_id = core.PofManager.add_meter_entry(event.dpid, 10000)
    
            core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])

            core.PofManager.set_port_of_enable(event.dpid, 1)
            core.PofManager.set_port_of_enable(event.dpid, 2)
            core.PofManager.set_port_of_enable(event.dpid, 3)
            
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            
            temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            temp_ins0 = core.PofManager.new_ins_meter(meter_id)
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 2)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            
            #print temp_ins1
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx], [temp_ins0,temp_ins1])        
    
            #=======================================================================
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
            
            action=core.PofManager.new_action_output(0, 0, 0, 0, 2)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            
            #print temp_ins1
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx], [temp_ins1])
            flag1 = 1
            
        if event.dpid==dpid2:

            core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            
            core.PofManager.set_port_of_enable(event.dpid, 1)
            core.PofManager.set_port_of_enable(event.dpid, 2)
            core.PofManager.set_port_of_enable(event.dpid, 3)
            
            temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 3)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            
            #print temp_ins1
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx], [temp_ins1])
            
            #=======================================================================
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 1)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            
            #print temp_ins1
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx], [temp_ins1])

            flag2 = 1
        
        #3 link east
        if event.dpid==dpid3:
            inport = pof.ofp_match20(field_name='inport',field_id=-1,offset=16,length=8)
            core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9), inport])

            core.PofManager.set_port_of_enable(event.dpid, 1)
            core.PofManager.set_port_of_enable(event.dpid, 2)
            core.PofManager.set_port_of_enable(event.dpid, 3)
            
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
            temp_matchx1 = core.PofManager.new_matchx(inport, '01', 'ff')
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            #temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
#core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx, temp_matchx1], [temp_ins1])    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 2)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx, temp_matchx1], [temp_ins1])
            #=======================================================================
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
            temp_matchx1 = core.PofManager.new_matchx(inport, '03', 'ff')
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            #temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 1)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx, temp_matchx1], [temp_ins1])

        #4 link dsp
        if event.dpid==dpid4:

            eth15_port = '02'
            eth14_port = '03'
            eth15_output = 2
            eth14_output = 3
            
            inport = pof.ofp_match20(field_name='inport',field_id=-1,offset=16,length=8)
            core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9), inport])

            core.PofManager.set_port_of_enable(event.dpid, 1)
            core.PofManager.set_port_of_enable(event.dpid, 2)
            core.PofManager.set_port_of_enable(event.dpid, 3)
            
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
            temp_matchx1 = core.PofManager.new_matchx(inport, eth15_port, 'ff')
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            #temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, 1)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx, temp_matchx1], [temp_ins1])
            #=======================================================================
            temp_matchx = core.PofManager.new_matchx(9, src2, 'ffffffff')
            temp_matchx1 = core.PofManager.new_matchx(inport, '01', 'ff')
            #core.PofManager.add_flow_table(event.dpid, 'SecondEntryTable', pof.OF_MM_TABLE, 128, [core.PofManager.get_field(9)])
            #temp_matchx = core.PofManager.new_matchx(9, src1, 'ffffffff')
            # temp_ins1 = core.PofManager.new_ins_goto_table(event.dpid,1)
    
            action=core.PofManager.new_action_output(0, 0, 0, 0, eth14_output)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])
            core.PofManager.add_flow_entry(event.dpid, 0, [temp_matchx, temp_matchx1], [temp_ins1])
            

def launch ():
    from pox.lib.recoco import Timer
    core.registerNew(TestCounter)
    Timer(1, _timer_func, recurring=True)
    #Timer(0.5, _timer_func1, recurring=True)
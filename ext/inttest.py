# -*- coding: UTF-8 -*-


from pox.core import core
from pox.lib.revent.revent import EventMixin
import pox.openflow.libpof_02 as of
import string
#from pox.lib.recoco import Timer
#from time import sleep



flow_entry_id_2291=0
dpid_231=2215152430
dpid_230=2215152298
#dpid_229=2215146867
tablenum = 20
num = 6000
device_map = {"SW1": 1,  # 191
              "SW2": 2,
              "SW3": 3,
              "SW4": 4
              # "SW4": 4,
              # "SW4": 4,# 192
              #"SW3": 2215152298,  # 230
              #"SW4": 2215152430,  # 231
              }

def _add_protocol(protocol_name, field_list):
    """
    Define a new protocol, and save it to PMDatabase.

    protocol_name: string
    field_list:[("field_name", length)]
    """
    match_field_list = []
    total_offset = 0
    for field in field_list:
        field_id = core.PofManager.new_field(field[0], total_offset, field[1])   #field[0]:field_name, field[1]:length
        total_offset += field[1]
        match_field_list.append(core.PofManager.get_field(field_id))
    core.PofManager.add_protocol(protocol_name, match_field_list)

def add_protocol():
    field_list = [("DMAC",48), ("SMAC",48), ("Eth_Type",16), ("V_IHL_TOS",16), ("Total_Len",16),
                  ("ID_Flag_Offset",32), ("TTL",8), ("Protocol",8), ("Checksum",16), ("SIP",32), ("DIP",32)]
    _add_protocol('ETH_IPv4', field_list)

    field_list = [("INT_DMAC", 48), ("INT_SMAC", 48), ("INT_Eth_Type", 16), ("INT_V_IHL_TOS", 16),
                  ("INT_Total_Len", 16),
                  ("INT_ID_Flag_Offset", 32), ("INT_TTL", 8), ("INT_Protocol", 8), ("INT_Checksum", 16),
                  ("INT_SIP", 32), ("INT_DIP", 32),
                  ("INT_SL", 16), ("INT_length", 8),
                  ("INT1_bitmap", 32), ("INT1_IP", 32), ("INT1_MAC", 48), ("INT1_port", 16),
                  ("INT2_bitmap", 32), ("INT2_IP", 32), ("INT2_MAC", 48), ("INT2_port", 16),
                  ("INT3_bitmap", 32), ("INT3_IP", 32), ("INT3_MAC", 48), ("INT3_port", 16),
                  ]
    _add_protocol('INT', field_list)

class Test(EventMixin):
    def __init__ (self):
        add_protocol()

        core.openflow.addListeners(self, priority=0)

    def _handle_ConnectionUp (self, event):

        if event.dpid == device_map["SW1"]:
            core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 32,
                                           [core.PofManager.get_field("Eth_Type")[0],
                                            core.PofManager.get_field("DIP")[0]])

            #  ------------------FirstEntryTable---------------------
            table_id = core.PofManager.get_flow_table_id(event.dpid, 'FirstEntryTable')  # 0
            match = core.PofManager.get_field("Eth_Type")[0]
            match2 = core.PofManager.get_field("DIP")[0]
            #  FF01表示INT协议
            temp_matchx = core.PofManager.new_matchx(match, '0800', 'FFFF')
            temp_matchx2 = core.PofManager.new_matchx(match2, 'c0a85902','FFFFFFFF')  # !!!中间这个参数改成目的IP!!!
            action = core.PofManager.new_action_add_field(field_id=0xffff, field_position=34*8, field_length=4*8, field_value='FFFFFFFF')
            temp_ins = core.PofManager.new_ins_apply_actions([action])  # goto Switch-0

            action = core.PofManager.new_action_output(0, 0, 0, 0, 0x2)
            temp_ins1 = core.PofManager.new_ins_apply_actions([action])  # goto Switch-0

            core.PofManager.add_flow_entry(event.dpid, table_id, [temp_matchx, temp_matchx2], [temp_ins, temp_ins1])

    def _handle_PortStatus(self, event):
        #print "yes, its the handle PortStatus fuction"
        port_id = event.ofp.desc.port_id
        port_name = event.ofp.desc.name
        if event.dpid == device_map.get("SW1"):
            if port_id == 0x1 or port_id == 0x2 or port_id == 0x3:
                core.PofManager.set_port_of_enable(event.dpid, port_id)
        if event.dpid == device_map.get("SW2"):
            if port_id == 0x1 or port_id == 0x2 or port_id == 0x3:
                core.PofManager.set_port_of_enable(event.dpid, port_id)
        # if event.dpid == device_map.get("SW3"):0
        #     if port_id == 0x1 or port_id == 0x2:
        #         core.PofManager.set_port_of_enable(event.dpid, port_id)
        # if event.dpid == device_map.get("SW4"):
        #     if port_id == 0x1 or port_id == 0x2:
        #         core.PofManager.set_port_of_enable(event.dpid, port_id)


def launch ():
    core.registerNew(Test)
    #Timer(25,change,recurring=False)
'''
Created on 2017.12.6

@author: xueyuhan
'''
from pox.core import core
from pox.lib.revent.revent import EventMixin
import pox.openflow.libpof_02 as of
from pox.lib.addresses import IPAddr,EthAddr
import time

def _add_protocol(protocol_name, field_list):
    """
    Define a new protocol, and save it to PMDatabase.

    protocol_name: string
    field_list:[("field_name", length)]
    """
    match_field_list = []
    total_offset = 0
    for field in field_list:
        field_id = core.PofManager.new_field(field[0], total_offset, field[1])   #field[0]:field_name, field[1]:length
        total_offset += field[1]
        match_field_list.append(core.PofManager.get_field(field_id))
    core.PofManager.add_protocol("protocol_name", match_field_list)

def add_protocol():
    field_list = [("DMAC",48), ("SMAC",48), ("Eth_Type",16), ("V_IHL_TOS",16), ("Total_Len",16),
                  ("ID_Flag_Offset",32), ("TTL",8), ("Protocol",8), ("Checksum",16), ("SIP_v4",32), ("DIP_v4",32)]
    _add_protocol('ETH_IPv4x', field_list)


class Test(EventMixin):
    def __init__ (self):
        add_protocol()
        core.openflow.addListeners(self, priority=0)

    def _handle_ConnectionUp (self, event):

        print "connection up!"

        for i in range(0,2):
            core.PofManager.set_port_of_enable(event.dpid, i, True)

        match_list=[]
        match_list.append(core.PofManager.get_field("SIP_v4")[0])
        table_id1=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 32, match_list)

        # match_list=[]
        # match_list.append(core.PofManager.get_field("SMAC")[0])
        # table_id2=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable2', of.OF_MM_TABLE, 4000, match_list)
        #
        # match_list=[]
        # match_list.append(core.PofManager.get_field("SMAC")[0])
        # table_id3=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable3', of.OF_MM_TABLE, 4000, match_list)
        #
        # match_list=[]
        # match_list.append(core.PofManager.get_field("SMAC")[0])
        # table_id4=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable3', of.OF_MM_TABLE, 4000, match_list)
        #
        # match_list=[]
        # match_list.append(core.PofManager.get_field("DMAC")[0])
        # table_id5=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable3', of.OF_MM_TABLE, 4000, match_list)


        ofinstructions=[]
        matchx_list=[]
        action_list=[]
        match = core.PofManager.get_field("SIP_v4")[0]
        temp_matchx2 = core.PofManager.new_matchx(match, '0a000001', 'FFFFFFFF')
        matchx_list.append(temp_matchx2)
        action=core.PofManager.new_action_output(0, 0, 0, 0, 1, 0)
        action_list.append(action)
        ofinstruction5=core.PofManager.new_ins_apply_actions(action_list)
        ofinstructions.append(ofinstruction5)
        flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id1,matchx_list,ofinstructions,1,1)

        # ofinstructions=[]
        # matchx_list=[]
        # action_list=[]
        # match = core.PofManager.get_field("DMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # action=core.PofManager.new_action_output(0, 0, 0, 0, 3, 0)
        # action_list.append(action)
        # ofinstruction=core.PofManager.new_ins_apply_actions(action_list)
        # ofinstructions.append(ofinstruction)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id5,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction1=core.PofManager.new_ins_goto_table(event.dpid, 1)
        # ofinstructions.append(ofinstruction1)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id1,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction2=core.PofManager.new_ins_goto_table(event.dpid, 1)
        # ofinstructions.append(ofinstruction2)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id1,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction3=core.PofManager.new_ins_goto_table(event.dpid, 2)
        # ofinstructions.append(ofinstruction3)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id2,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 2)
        # ofinstructions.append(ofinstruction4)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id2,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction3=core.PofManager.new_ins_goto_table(event.dpid, 3)
        # ofinstructions.append(ofinstruction3)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id3,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 3)
        # ofinstructions.append(ofinstruction4)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id3,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction3=core.PofManager.new_ins_goto_table(event.dpid, 4)
        # ofinstructions.append(ofinstruction3)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id4,matchx_list,ofinstructions,1,1)
        #
        # ofinstructions=[]
        # matchx_list=[]
        # match = core.PofManager.get_field("SMAC")[0]
        # temp_matchx2 = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
        # matchx_list.append(temp_matchx2)
        # ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 4)
        # ofinstructions.append(ofinstruction4)
        # flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id4,matchx_list,ofinstructions,1,1)
'''
        if event.dpid == 2:

            for i in range(1,3):
                core.PofManager.set_port_of_enable(event.dpid, i, True)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id6=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id7=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id8=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id9=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("DMAC")[0])
            table_id10=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("DMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 1)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id10,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("DMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 1)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id10,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 2)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id7,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 2)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id7,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 3)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id8,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 3)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id8,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 4)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id9,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 4)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id9,matchx_list,ofinstructions,1,1)

            action_list=[]
            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            action=core.PofManager.new_action_output(0, 0, 0, 0, 1, 0)
            action_list.append(action)
            ofinstruction=core.PofManager.new_ins_apply_actions(action_list)
            ofinstructions.append(ofinstruction)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id6,matchx_list,ofinstructions,1,1)

            action_list=[]
            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            action=core.PofManager.new_action_output(0, 0, 0, 0, 3, 0)
            action_list.append(action)
            ofinstruction=core.PofManager.new_ins_apply_actions(action_list)
            ofinstructions.append(ofinstruction)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id6,matchx_list,ofinstructions,1,1)
        if event.dpid == 1:

            for i in range(1,2):
                core.PofManager.set_port_of_enable(event.dpid, i, True)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id6=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id7=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id8=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id9=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            match_list=[]
            match_list.append(core.PofManager.get_field("SMAC")[0])
            table_id10=core.PofManager.add_flow_table(event.dpid, 'FirstEntryTable', of.OF_MM_TABLE, 500, match_list)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 1)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id10,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 1)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id10,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 2)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id7,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 2)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id7,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 3)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id8,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 3)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id8,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 4)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id9,matchx_list,ofinstructions,1,1)

            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            ofinstruction4=core.PofManager.new_ins_goto_table(event.dpid, 4)
            ofinstructions.append(ofinstruction4)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id9,matchx_list,ofinstructions,1,1)

            action_list=[]
            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, 'f80f41f66340', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            action=core.PofManager.new_action_output(0, 0, 0, 0, 2, 0)
            action_list.append(action)
            ofinstruction=core.PofManager.new_ins_apply_actions(action_list)
            ofinstructions.append(ofinstruction)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id6,matchx_list,ofinstructions,1,1)

            action_list=[]
            ofinstructions=[]
            matchx_list=[]
            match = core.PofManager.get_field("SMAC")[0]
            temp_matchx = core.PofManager.new_matchx(match, '70e284088aab', 'FFFFFFFFFFFF')
            matchx_list.append(temp_matchx)
            action=core.PofManager.new_action_output(0, 0, 0, 0, 1, 0)
            action_list.append(action)
            ofinstruction=core.PofManager.new_ins_apply_actions(action_list)
            ofinstructions.append(ofinstruction)
            flow_entry_idd=core.PofManager.add_flow_entry(event.dpid,table_id6,matchx_list,ofinstructions,1,1)
            '''



def launch ():
    core.registerNew(Test)

